
import telebot
import random
import os
import time
import psutil
bot = telebot.TeleBot('5915775889:AAHbhaQcgufd-OMjQfZ6jNH_Df0jA9k66r8')
dir = "Photos"
dir2 = "Audios"
FLAG = 0

@bot.message_handler(commands=["start"])
def start(message, res=False):
    bot.send_message(message.chat.id, 'Приветствую. Данный бот отправляет уведомления')
    bot.send_message(message.chat.id, "Пожалуйста, выберите тип уведомления, который хотите настроить")

@bot.message_handler(commands=["Фото", "фото"]) #отправляет фото по времени пользователя
def set_photo(message):
    bot.send_message(message.chat.id, text="Введите время, в которое вы бы хотели получать уведомления. Пример - 21:30")
    global FLAG
    FLAG = 1

@bot.message_handler(commands=["Аудио", "аудио"]) #отправляет аудио по времени пользователя
def set_audio(message):
    bot.send_message(message.chat.id, text="Введите время, в которое вы бы хотели получать уведомления. Пример - 21:30")
    global FLAG
    FLAG = 2

@bot.message_handler(commands=["Видео", "видео"]) #отправляет видео, если введенный процесс запущен от 0 часов ночи до 6 часов утра
def set_video(message):
    bot.send_message(message.chat.id, text="Введите название процесса, при запуске которого будет приходить уведомление. Пример - cmd.exe")
    global FLAG
    FLAG = 3

@bot.message_handler(commands=["help"])
def help(message):
    bot.send_message(message.chat.id, text="Бот предлагает следующие команды: \n\nфото - настроить время(время ЕКБ), когда будут приходить пожелания с добрым утром\n\nвидео - бот присылает видео предупреждение, когда выбранный пользователем процесс заупскается\n\nаудио - настроить время(время ЕКБ), когда будут приходить песни группы ABBA\n\noff - отключение всех уведомлений")

@bot.message_handler(commands=["off"])
def off(message):
    global FLAG
    FLAG=0

@bot.message_handler(content_types=["text"])
def push_sms(message):
    if FLAG==1 or FLAG==2:
        try:
            hour1, min1 = message.text.split(':')
            hour = int(hour1)
            min = int(min1)
            if hour > 24 or hour < 0 or min > 60 or min < 0:
                raise ValueError
            if FLAG == 1:
                while FLAG:
                    photo = open(os.path.join(dir, random.choice(os.listdir(dir))), "rb")
                    morning = time.localtime()
                    if morning.tm_hour == hour and morning.tm_min == min:
                        bot.send_photo(message.chat.id, photo)
                    time.sleep(86400)
            elif FLAG == 2:
                while FLAG:
                    audio = open(os.path.join(dir2, random.choice(os.listdir(dir2))), "rb")
                    morning = time.localtime()
                    if morning.tm_hour == hour and morning.tm_min == min:
                        bot.send_audio(message.chat.id, audio)
                    time.sleep(86400)
        except ValueError:
            bot.send_message(message.chat.id, text="Введите корректное значение времени")
    elif FLAG==3:
        while FLAG:
            for proc in psutil.process_iter():
                name = proc.name()
                if name == message.text and (time.localtime().tm_hour >= 0 or time.localtime().tm_hour <= 6):
                    bot.send_video(message.chat.id, video=open("Video/Danger.mp4", 'rb'))
                    time.sleep(60)

if __name__ == '__main__':
    while True:
        try:
            bot.polling(none_stop=True)
        except Exception as ex:
            print(ex)